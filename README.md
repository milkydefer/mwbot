mwbot-rs
========
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

mwbot-rs is a framework for writing bots and tools in Rust. The aim is to
provide the building blocks for people to write safe and concurrent
applications that interact with and enhance MediaWiki. The project is still
getting started, all contributions are welcome! 

See the [mwbot-rs wiki page](https://www.mediawiki.org/wiki/mwbot-rs) for more
details.

## Crates

| Crate                  | Version                                                                                                                     | Documentation                                                                                              |
|------------------------|-----------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| mwbot                  | [![crates.io](https://img.shields.io/crates/v/mwbot.svg)](https://crates.io/crates/mwbot)                                   | [![docs.rs](https://img.shields.io/docsrs/mwbot)](https://docs.rs/mwbot)                                   |
| mwbot_derive           | [![crates.io](https://img.shields.io/crates/v/mwbot_derive.svg)](https://crates.io/crates/mwbot_derive)                     | [![docs.rs](https://img.shields.io/docsrs/mwbot_derive)](https://docs.rs/mwbot_derive)                     |
| mwapi                  | [![crates.io](https://img.shields.io/crates/v/mwapi.svg)](https://crates.io/crates/mwapi)                                   | [![docs.rs](https://img.shields.io/docsrs/mwapi)](https://docs.rs/mwapi)                                   |
| mwapi_responses        | [![crates.io](https://img.shields.io/crates/v/mwapi_responses.svg)](https://crates.io/crates/mwapi_responses)               | [![docs.rs](https://img.shields.io/docsrs/mwapi_responses)](https://docs.rs/mwapi_responses)               |
| mwapi_responses_derive | [![crates.io](https://img.shields.io/crates/v/mwapi_responses_derive.svg)](https://crates.io/crates/mwapi_responses_derive) | [![docs.rs](https://img.shields.io/docsrs/mwapi_responses_derive)](https://docs.rs/mwapi_responses_derive) |
| mwseaql                | [![crates.io](https://img.shields.io/crates/v/mwseaql.svg)](https://crates.io/crates/mwapi_responses_derive)                | [![docs.rs](https://img.shields.io/docsrs/mwseaql)](https://docs.rs/mwseaql)                               |
| mwtimestamp            | [![crates.io](https://img.shields.io/crates/v/mwtimestamp.svg)](https://crates.io/crates/mwtimestamp)                       | [![docs.rs](https://img.shields.io/docsrs/mwtimestamp)](https://docs.rs/mwtimestamp)                       |
| mwtitle                | [![crates.io](https://img.shields.io/crates/v/mwtitle.svg)](https://crates.io/crates/mwtitle)                               | [![docs.rs](https://img.shields.io/docsrs/mwtitle)](https://docs.rs/mwtitle)                               |
| parsoid                | [![crates.io](https://img.shields.io/crates/v/parsoid.svg)](https://crates.io/crates/parsoid)                               | [![docs.rs](https://img.shields.io/docsrs/parsoid)](https://docs.rs/parsoid)                               |

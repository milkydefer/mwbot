# mwapi

[![crates.io](https://img.shields.io/crates/v/mwapi.svg)](https://crates.io/crates/mwapi)
[![docs.rs](https://img.shields.io/docsrs/mwapi?label=docs.rs)](https://docs.rs/mwapi)
[![docs (main)](https://img.shields.io/badge/doc.wikimedia.org-green?label=docs%40main)](https://doc.wikimedia.org/mwbot-rs/mwbot/mwapi/)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

A MediaWiki API client library.

`mwapi` is a low-level library for the [MediaWiki Action API](https://www.mediawiki.org/wiki/API:Main_page).
If you intend to edit pages or want a higher-level interface, it's recommended to use [`mwbot`](https://docs.rs/mwbot/),
which builds on top of this crate.

### Goals
* generic to fit any application, whether for interactive usage
  or writing a bot
* fully compatible with concurrent use cases
* turns MediaWiki errors into Rust errors for you
* logging (using the `tracing` crate) for visiblity into errors
* follow all [best practices](https://www.mediawiki.org/wiki/API:Etiquette)

### Quick start
```rust
let client = mwapi::Client::builder("https://en.wikipedia.org/w/api.php")
    .set_user_agent("mwapi demo")
    // Provide credentials for login:
    // .set_botpassword("username", "password")
    .build().await?;
let resp = client.get_value(&[
    ("action", "query"),
    ("prop", "info"),
    ("titles", "Taylor Swift"),
]).await?;
let info = resp["query"]["pages"][0].clone();
assert_eq!(info["ns"].as_u64().unwrap(), 0);
assert_eq!(info["title"].as_str().unwrap(), "Taylor Swift");

```

### Functionality
* authentication, using [OAuth2](https://www.mediawiki.org/wiki/OAuth/Owner-only_consumers#OAuth_2) (recommended) or BotPasswords
* error handling, transforming MediaWiki errors into Rust ones
* CSRF [token](https://www.mediawiki.org/wiki/API:Tokens) handling with `post_with_token`
* rate limiting and concurrency controls
* file uploads (needs `upload` feature)

### See also
* [`mwbot`](https://docs.rs/mwbot/) provides a higher level interface to
  interacting with MediaWiki
* [`mwapi_responses`](https://docs.rs/mwapi_responses/) is a macro to
  generate strict types for dynamic API queries

### Contributing
`mwapi` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
if you're interested!

## License
This crate is released under GPL-3.0-or-later.
See [COPYING](./COPYING) for details.

// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
//! This crate provides the proc_macro for [`mwbot`](https://docs.rs/mwbot/),
//! please refer to its documentation for usage.
#![deny(clippy::all)]
#![deny(rustdoc::all)]

use proc_macro::TokenStream;
use proc_macro2::{Ident, TokenStream as TokenStream2};
use quote::quote;
use std::collections::HashMap;
use syn::spanned::Spanned;
use syn::{
    parse_macro_input, Data, DeriveInput, Error, Expr, Field, Fields,
    GenericArgument, Lit, LitStr, PathArguments, Result, Type,
};

#[proc_macro_derive(Generator, attributes(params, param, generator))]
pub fn generator(input: TokenStream) -> TokenStream {
    // Parse the input tokens into a syntax tree
    let input = parse_macro_input!(input as DeriveInput);
    let tokens = build(input).unwrap_or_else(|err| err.into_compile_error());
    tokens.into()
}

fn build(input: DeriveInput) -> Result<TokenStream2> {
    let name = &input.ident;
    let params = parse(&input)?;
    let fixed_params = parse_fixed_params(&input)?;
    let generator_option = parse_generator_option(&input)?;
    //dbg!(&params);
    let plain_impl = build_impl(name, &params);
    //println!("{}", &plain_impl);
    let trait_impl =
        build_trait_impl(name, &params, fixed_params, generator_option);

    // Build the output, possibly using quasi-quotation
    let expanded = quote! {
        #plain_impl
        #trait_impl
    };
    Ok(expanded)
}

struct Parameter {
    rust_name: Ident,
    rust_type: Type,
    doc: Option<LitStr>,
    mw_name: LitStr,
    required: bool,
}

impl Parameter {
    /// Whether rust_type is "bool"
    fn is_boolean(&self) -> bool {
        if let Type::Path(type_path) = &self.rust_type {
            if let Some(first) = type_path.path.segments.first() {
                return first.ident == "bool";
            }
        }

        false
    }
}

/// Peeks through Option<T> to determine the underlying type and if its required
/// Given `Option<foo>`, return `(foo, false)`
/// Given `foo`, return `(foo, true)`
fn parse_through_option(ty: &Type) -> (Type, bool) {
    if let Type::Path(type_path) = ty {
        if let Some(first) = type_path.path.segments.first() {
            if first.ident == "Option" {
                if let PathArguments::AngleBracketed(inside) = &first.arguments
                {
                    if let Some(GenericArgument::Type(ty)) = inside.args.first()
                    {
                        return (ty.clone(), false);
                    }
                }
            }
        }
    }
    (ty.clone(), true)
}

/// Build the `Generator` trait implementation
fn build_trait_impl(
    name: &Ident,
    params: &[Parameter],
    fixed_params: HashMap<String, LitStr>,
    generator_option: GeneratorOption,
) -> TokenStream2 {
    let fixed_params: Vec<_> = fixed_params
        .into_iter()
        .map(|(key, value)| {
            quote! {
                map.insert(#key, #value.to_string());
            }
        })
        .collect();
    let params: Vec<_> = params
        .iter()
        .map(|param| {
            let mw_name = &param.mw_name;
            let rust_name = &param.rust_name;
            if param.required {
                quote! {
                    map.insert(#mw_name, crate::generators::ParamValue::stringify(&self.#rust_name));
                }
            } else if param.is_boolean() {
                // For boolean parameters, include it if it's true, otherwise
                // omit it entirely.
                quote! {
                    if self.#rust_name.unwrap_or(false) {
                        map.insert(#mw_name, "1".to_string());
                    }
                }
            } else {
                quote! {
                    if let Some(value) = &self.#rust_name {
                        map.insert(#mw_name, crate::generators::ParamValue::stringify(value));
                    }
                }
            }
        })
        .collect();
    let return_type = generator_option.return_type;
    let response_type = generator_option.response_type;
    let transform_fn = generator_option.transform_fn;

    quote! {
        impl crate::generators::Generator for #name {
            type Output = crate::Result<#return_type>;

            fn params(&self) -> ::std::collections::HashMap<&'static str, String> {
                let mut map = ::std::collections::HashMap::new();
                #(#fixed_params)*
                #(#params)*
                map
            }

            fn generate(self, bot: &crate::Bot) -> ::tokio::sync::mpsc::Receiver<Self::Output> {
                let (tx, rx) = ::tokio::sync::mpsc::channel(50);
                let bot = bot.clone();
                tokio::spawn(async move {
                    let mut params = crate::generators::Params {
                        main: self.params()
                            .into_iter()
                            .map(|(k, v)| (k.to_string(), v))
                            .collect(),
                        ..Default::default()
                    };
                    loop {
                        let resp: #response_type =
                            match mwapi_responses::query_api(&bot.api, params.merged())
                                .await
                            {
                                Ok(resp) => resp,
                                Err(err) => {
                                    match tx.send(Err(crate::Error::from(err))).await {
                                        Ok(_) => break,
                                        // Receiver hung up, just abort
                                        Err(_) => return,
                                    }
                                }
                            };
                        params.continue_ = resp.continue_.clone();
                        for item in mwapi_responses::ApiResponse::<_>::into_items(resp) {
                            let value = #transform_fn(&bot, item);
                            if tx.send(value).await.is_err() {
                                // Receiver hung up, just abort
                                return;
                            }
                        }
                        if params.continue_.is_empty() {
                            // All done
                            break;
                        }
                    }
                });
                rx
            }
        }
    }
}

/// Build the type's implementation with `new()` and parameter builders
fn build_impl(name: &Ident, params: &[Parameter]) -> TokenStream2 {
    let type_def: Vec<_> = params
        .iter()
        .filter(|param| param.required)
        .map(|param| {
            let name = &param.rust_name;
            let ty = &param.rust_type;
            quote! { #name: impl Into<#ty> }
        })
        .collect();
    let fields: Vec<_> = params
        .iter()
        .map(|param| {
            let name = &param.rust_name;
            if param.required {
                quote! { #name: #name.into() }
            } else {
                quote! { #name: None }
            }
        })
        .collect();

    let setters: Vec<_> = params
        .iter()
        .filter(|param| !param.required)
        .map(setter)
        .collect();

    quote! {
        impl #name {
            pub fn new( #(#type_def),* ) -> Self {
                Self {
                    #(#fields),*
                }
            }

            #(#setters)*
        }
    }
}

/// Build a parameter setter function
fn setter(param: &Parameter) -> TokenStream2 {
    let name = &param.rust_name;
    let ty = &param.rust_type;
    let doc = match &param.doc {
        Some(doc) => quote! {
            #[doc = #doc]
        },
        None => quote! {},
    };
    assert!(!param.required);
    quote! {
        #doc
        pub fn #name(mut self, value: impl Into<#ty>) -> Self {
            self.#name = Some(value.into());
            self
        }
    }
}

/// Parse Rust struct types into our `Parameter` type
fn parse(input: &DeriveInput) -> Result<Vec<Parameter>> {
    let mut params = vec![];
    let data = if let Data::Struct(data) = &input.data {
        data
    } else {
        return Err(Error::new(input.ident.span(), "expected a struct"));
    };
    let fields = if let Fields::Named(fields) = &data.fields {
        fields
    } else {
        return Err(Error::new(
            input.ident.span(),
            "struct fields must have names",
        ));
    };
    for field in &fields.named {
        let (rust_type, required) = parse_through_option(&field.ty);

        let (doc, mw_name) = parse_mw_name(field)?;
        let param = Parameter {
            rust_name: field.ident.clone().ok_or_else(|| {
                Error::new(field.span(), "struct fields must have names")
            })?,
            doc,
            mw_name,
            rust_type,
            required,
        };
        params.push(param);
    }
    Ok(params)
}

/// Parse the generator attribute
fn parse_fixed_params(input: &DeriveInput) -> Result<HashMap<String, LitStr>> {
    let mut map = HashMap::new();
    for attr in &input.attrs {
        if attr.path().is_ident("params") {
            attr.parse_nested_meta(|meta| {
                let key = meta
                    .path
                    .get_ident()
                    .ok_or_else(|| meta.error("invalid parameter name"))?;
                let value: LitStr = meta.value()?.parse()?;
                map.insert(key.to_string(), value);
                Ok(())
            })?;
        }
    }
    if !map.keys().any(|key| key == "generator" || key == "list") {
        Err(Error::new(
            input.ident.span(),
            "Missing #[params(generator = \"...\")] or #[params(list = \" ... \")] attribute",
        ))
    } else {
        Ok(map)
    }
}

struct GeneratorOption {
    return_type: Type,
    response_type: Type,
    transform_fn: Type,
}

fn parse_generator_option(input: &DeriveInput) -> Result<GeneratorOption> {
    let mut return_type: Option<Type> = None;
    let mut response_type: Option<Type> = None;
    let mut transform_fn: Option<Type> = None;
    for attr in &input.attrs {
        if attr.path().is_ident("generator") {
            attr.parse_nested_meta(|meta| {
                if meta.path.is_ident("return_type") {
                    return_type = Some(meta.value()?.parse()?);
                    return Ok(());
                }
                if meta.path.is_ident("response_type") {
                    response_type = Some(meta.value()?.parse()?);
                    return Ok(());
                }
                if meta.path.is_ident("transform_fn") {
                    transform_fn = Some(meta.value()?.parse()?);
                    return Ok(());
                }
                Ok(())
            })?;
        }
    }
    Ok(GeneratorOption {
        return_type: return_type.unwrap_or(syn::parse_quote!(crate::Page)),
        response_type: response_type
            .unwrap_or(syn::parse_quote!(crate::page::InfoResponse)),
        transform_fn: transform_fn
            .unwrap_or(syn::parse_quote!(crate::generators::transform_to_page)),
    })
}

/// Parse the parameter attribute
fn parse_mw_name(field: &Field) -> Result<(Option<LitStr>, LitStr)> {
    let mut doc = None;
    let mut param = None;
    for attr in &field.attrs {
        if attr.path().is_ident("doc") {
            if let Expr::Lit(expr) = &attr.meta.require_name_value()?.value {
                if let Lit::Str(lit) = &expr.lit {
                    doc = Some(lit.clone());
                }
            }
            // else: error?
        } else if attr.path().is_ident("param") {
            let parsed: LitStr = attr.parse_args()?;
            param = Some(parsed);
        }
    }
    match param {
        Some(param) => Ok((doc, param)),
        None => Err(Error::new(
            field.ident.span(),
            "Missing #[param(\"...\")] attribute",
        )),
    }
}

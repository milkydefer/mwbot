# mwtitle

[![crates.io](https://img.shields.io/crates/v/mwtitle.svg)](https://crates.io/crates/mwtitle)
[![docs.rs](https://img.shields.io/docsrs/mwtitle?label=docs.rs)](https://docs.rs/mwtitle)
[![docs (main)](https://img.shields.io/badge/doc.wikimedia.org-green?label=docs%40main)](https://doc.wikimedia.org/mwbot-rs/mwbot/mwtitle/)
[![pipeline status](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/badges/main/pipeline.svg)](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot/-/commits/main)
[![coverage report](https://img.shields.io/endpoint?url=https%3A%2F%2Fdoc.wikimedia.org%2Fcover%2Fmwbot-rs%2Fmwbot%2Fcoverage%2Fcoverage.json)](https://doc.wikimedia.org/cover/mwbot-rs/mwbot/coverage)

`mwtitle` is a library for parsing, normalizing and formatting MediaWiki
page titles. It is primarily a port of the MediaWikiTitleCodec class
from MediaWiki, and passes the MediaWiki test suite.

The easiest way to get started is create a [`TitleCodec`] from a [siteinfo](https://www.mediawiki.org/wiki/API:Siteinfo)
API request.
```rust
let url = "https://en.wikipedia.org/w/api.php\
           ?action=query&meta=siteinfo\
           &siprop=general|namespaces|namespacealiases|interwikimap\
           &formatversion=2&format=json";
let resp: SiteInfoResponse = reqwest::get(url).await?.json().await?;
let codec = TitleCodec::from_site_info(resp.query)?;
let title = codec.new_title("Talk:Main Page#Section 1")?;
assert_eq!(title.namespace(), 1);
assert_eq!(title.dbkey(), "Main_Page");
assert_eq!(title.fragment(), Some("Section 1"));
assert_eq!(codec.to_pretty(&title), "Talk:Main Page".to_string());
assert_eq!(
    codec.to_pretty_with_fragment(&title),
    "Talk:Main Page#Section 1".to_string()
);
```

It's also possible to possible to create a `TitleCodec` from a JSON
`siteinfo-namespaces.json` or compressed `siteinfo-namespaces.json.gz`
that comes from Wikimedia dumps. This requires the extra `utils` feature
to be enabled.

### Contributing
`mwtitle` is a part of the [`mwbot-rs` project](https://www.mediawiki.org/wiki/Mwbot-rs).
We're always looking for new contributors, please [reach out](https://www.mediawiki.org/wiki/Mwbot-rs#Contributing)
if you're interested!

## License
This crate is released under GPL-3.0-or-later.
See [COPYING](./COPYING) for details.

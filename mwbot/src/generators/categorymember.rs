// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use super::ParamValue;

pub enum CategoryMemberType {
    Page,
    Subcat,
    File,
    All,
}

impl ParamValue for CategoryMemberType {
    fn stringify(&self) -> String {
        match self {
            Self::Page => "page",
            Self::Subcat => "subcat",
            Self::File => "file",
            Self::All => "page|subcat|file",
        }
        .to_string()
    }
}

impl Default for CategoryMemberType {
    fn default() -> Self {
        Self::All
    }
}

pub enum CategoryMemberSort {
    Sortkey,
    Timestamp,
}

impl ParamValue for CategoryMemberSort {
    fn stringify(&self) -> String {
        match self {
            Self::Sortkey => "sortkey",
            Self::Timestamp => "timestamp",
        }
        .to_string()
    }
}

impl Default for CategoryMemberSort {
    fn default() -> Self {
        Self::Sortkey
    }
}

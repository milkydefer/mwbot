use mwbot::generators::Generator;

#[derive(Generator)]
#[params(generator = "foo")]
enum Enum {
    One,
    Two,
}

fn main() {}
